<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldset_Email</name>
   <tag></tag>
   <elementGuidId>a613425d-d3fd-4c66-968c-b86723f5fa0b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Content']/div/div[2]/article/div/div/div/div[2]/form/fieldset</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>fieldset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-form group</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    
                                        Email
                                        
                                    
                                    
                                        Password
                                        
                                    
                                    
                                    
                                                                             
                                    

                                    
                                        Sign In
                                    

                                    
Forgot Password?                                    

                                        
                                            Register Your Account
                                        
                                    
                                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Content&quot;)/div[@class=&quot;zone zone-content&quot;]/div[@class=&quot;C04 container&quot;]/article[@class=&quot;widget-html-widget widget&quot;]/div[@class=&quot;ML pg-loginWrap&quot;]/div[@class=&quot;MR&quot;]/div[@class=&quot;MM&quot;]/div[@class=&quot;logonForm&quot;]/form[1]/fieldset[@class=&quot;login-form group&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='Content']/div/div[2]/article/div/div/div/div[2]/form/fieldset</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log On'])[1]/following::fieldset[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimum Wage Monitor'])[1]/following::fieldset[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//fieldset</value>
   </webElementXpaths>
</WebElementEntity>
