<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_.afill000opacity0.65.bfill</name>
   <tag></tag>
   <elementGuidId>a2cf2a6e-6332-45ff-a0b3-2e02587ceddd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='player']/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fp-ui</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>                    
  
  
    
      
  
  
    
      
  
  
    
      
  
  
    
      
  

           
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  

           
    
    
      
      
    
    
      
      
    
    
      
      
    
    
      
      
    

           
    
    
      
      
    
    
      
      
    
    
      
      
    
    
      
      
    

                                                                                                    
  .a{fill:#000;opacity:0.65;}.b{fill:#fff;opacity:1.0;}
  play-rounded-fill
  
  
  
  
           .fp-color-play{opacity:0.65;}.controlbutton{fill:#fff;}

play-rounded-outline
           
  
    .fp-color-play{opacity:0.65;}.controlbutton{fill:#fff;}
  
  play-sharp-fill
  
  

           .controlbuttonbg{opacity:0.65;}.controlbutton{fill:#fff;}
play-sharp-outline
                                        .fp-color-play{opacity:0.65;}.rect{fill:#fff;}
pause-sharp-outline
           .fp-color-play{opacity:0.65;}.rect{fill:#fff;}
pause-sharp-fill
           .fp-color-play{opacity:0.65;}.rect{fill:#fff;}
pause-rounded-outline
           .fp-color-play{opacity:0.65;}.rect{fill:#fff;}
pause-rounded-fill
                                          00:00                                                                                                                                                                                         ShareTwitterEmbed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;player&quot;)/div[@class=&quot;fp-player&quot;]/div[@class=&quot;fp-ui&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='player']/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn how to manage your services at Poster Guard Online'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Tutorials'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
