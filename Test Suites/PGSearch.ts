<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PGSearch</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3e985756-e282-428a-a9cc-a24b5b96d253</testSuiteGuid>
   <testCaseLink>
      <guid>9f220a69-01e2-4328-a485-0988ad6c05e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Resources</testCaseId>
      <testDataLink>
         <combinationType>MANY</combinationType>
         <id>4225be4c-bbec-47f3-a949-b1941c973f34</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-8</value>
         </iterationEntity>
         <testDataId>Data Files/Full Search</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>MANY</combinationType>
         <id>19f1cbc9-5e36-47f7-b746-76073da715a0</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>12-20</value>
         </iterationEntity>
         <testDataId>Data Files/Full Search</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>
