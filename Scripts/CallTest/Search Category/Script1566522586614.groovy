import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Welcome/a_Account Administrators'))

for (def rowNum = 1; rowNum <= findTestData('Data1').getRowNumbers(); rowNum++) {
    //WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 'lou.gill000@gmail.com')
    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameorEmail'), 
        findTestData('Data1').getValue(1, rowNum))

    WebUI.delay(3)

    //WebUI.setEncryptedText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'), '3Z9vEXbPUsDWIpYU74WqmA==')
    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'), 
        findTestData('Data1').getValue(2, rowNum))

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/button_Sign In'))

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Dashboard/input_Delivery Address_Custome'))

    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Dashboard/input_Delivery Address_Custome'), 
        '2424 Pike Street San Diego, CA  92101')

    WebUI.click(findTestObject('SearchShippingaddress_category/Page_PosterGuard - Dashboard/a_Delivery Address'))

    WebUI.click(findTestObject('SearchShippingaddress_category/Page_PosterGuard - Dashboard/i_Delivery Address_fa fa-searc'))

    WebUI.doubleClick(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Dashboard/td_Brookstone  San Diego Intl.'))

    WebUI.delay(20)

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/input_PG Service ID_ServiceSea'))

    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/input_PG Service ID_ServiceSea'), 
        '382020')

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/a_PG Service ID'))

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/i_PG Service ID_fa fa-search'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.delay(10)

    WebUI.click(findTestObject('CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'))

    WebUI.setText(findTestObject('CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'), '180')

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/a_Custom ID 1  2'))

    WebUI.delay(5)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_PG Service ID_fa fa-search'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.delay(8)
	
	WebUI.click(findTestObject('CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'))
	
	WebUI.setText(findTestObject('CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'), 'West')
	
    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/a_Custom ID 1  2'))

    WebUI.delay(5)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_PG Service ID_fa fa-search'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.delay(10)
	
	WebUI.click(findTestObject('CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'))

    WebUI.setText(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/input_PG Service ID_ServiceSea'), 'Brookstone Dallas Galleria')

    WebUI.delay(5)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/a_Delivery Company'))

    WebUI.delay(5)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_PG Service ID_fa fa-search'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_PG Service ID_fa fa-search'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/button_Active'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/CategorySearch/Page_PosterGuard/button_Cancelled'))

	WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/a_Sign Out'))
}

WebUI.closeBrowser()

