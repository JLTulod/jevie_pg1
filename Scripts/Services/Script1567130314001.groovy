import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Services/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 'lou.gill000@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Services/Page_PosterGuard - Log On/input_Password_password'), '3Z9vEXbPUsDWIpYU74WqmA==')

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard - Log On/button_Sign In'))

WebUI.setText(findTestObject('Object Repository/Services/Page_PosterGuard - Dashboard/input_Delivery Address_Custome'), 
    '450289')

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard - Dashboard/a_PG Service ID'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard - Dashboard/i_Delivery Address_fa fa-searc'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Active'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Active'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Cancelled'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Cancelled'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/li_Select Option'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/div_MA'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/div_MA'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/p_State  Province'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/div_LA'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/div_LA'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/p_CityCounty'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Miami Beach'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Miami Beach'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/p_Additional Services'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_HotelRestaurant'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_HotelRestaurant'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/li_Language'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_English'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_English'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/p_Service Format'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/button_Poster'))

WebUI.click(findTestObject('Object Repository/Services/Page_PosterGuard/a_Sign Out'))

WebUI.closeBrowser()

