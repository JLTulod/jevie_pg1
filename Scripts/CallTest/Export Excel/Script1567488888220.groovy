import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

Robot rb = new Robot()

WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Welcome/a_Account Administrators'))

for (def rowNum = 1; rowNum <= findTestData('Data1').getRowNumbers(); rowNum++) {
    //WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 'lou.gill000@gmail.com')
    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameorEmail'), 
        findTestData('Data1').getValue(1, rowNum))

    WebUI.delay(3)

    //WebUI.setEncryptedText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'), '3Z9vEXbPUsDWIpYU74WqmA==')
    WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'), 
        findTestData('Data1').getValue(2, rowNum))

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/button_Sign In'))

    WebUI.maximizeWindow()

    WebUI.delay(3)

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard - Dashboard/input_Delivery Address_Custome'), 
        'A02354921')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard - Dashboard/a_Account'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard - Dashboard/i_Delivery Address_fa fa-searc'))

    WebUI.doubleClick(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard - Dashboard/td_SAMSUNG ELECTRONICS AMERICA'))

    WebUI.delay(15)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Export to Excel'))

    rb.keyPress(KeyEvent.VK_ENTER)

    WebUI.delay(5)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Export to Excel'))

    rb.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(20)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_SHIPMENTS'))

    WebUI.delay(15)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))
	
	WebUI.delay(2)

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), '320213')
	
	WebUI.delay(5)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/a_PG Service ID'))
	
	WebUI.delay(2)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'A0235491')

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/a_Account'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'Marjorie')

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/a_Delivery First Name'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'Park')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Delivery Last Name'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'Home Office')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Custom ID 1  2'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))

	WebUI.delay(8)
	
    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'Fed Con')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Custom ID 1  2'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_Sales Order _ShipmentsSe'), 'SO-09893964')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Sales Order'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Sales Order _fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_INVOICES'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Export to Excel'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Export to Excel_1'))

	rb.keyPress(KeyEvent.VK_ENTER)
	
	WebUI.delay(5)
	
	rb.keyRelease(KeyEvent.VK_ENTER)
	
	WebUI.delay(20)
		
    WebUI.setText(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/input_PG Service ID_InvoicesSe'), 'INV7928873')

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/a_Invoice'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_PG Service ID_fa fa-search'))
	
	WebUI.delay(8)

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/i_Clear Search_fa fa-times-cir'))

	WebUI.delay(5)
	
    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/li_Search by Category'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Past Due'))
	
    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/button_Past Due'))
	
	WebUI.delay(5)
	

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Unpaid'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/button_Unpaid'))
	
	WebUI.delay(5)
	

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Paid'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/button_Paid'))

	WebUI.delay(5)
	
	
    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/p_Closed'))

    WebUI.click(findTestObject('Object Repository/Alloutsearch/Page_PosterGuard/button_Closed'))
	
	WebUI.delay(5)
	

    WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/a_Sign Out'))

    WebUI.closeBrowser()
}

