import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Welcome/a_Account Administrators'))

for (def rowNum = 1; rowNum <= findTestData('Data1').getRowNumbers(); rowNum++) {
	//WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 'lou.gill000@gmail.com')
	WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Email_userNameorEmail'),
		findTestData('Data1').getValue(1, rowNum))

	WebUI.delay(3)

	//WebUI.setEncryptedText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'), '3Z9vEXbPUsDWIpYU74WqmA==')
	WebUI.setText(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/input_Password_password'),
		findTestData('Data1').getValue(2, rowNum))

	WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard - Log On/button_Sign In'))
	
	WebUI.maximizeWindow()
	
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/SearchShippingaddress_category/Page_PosterGuard/a_Sign Out'))
	
	WebUI.closeBrowser()
}

